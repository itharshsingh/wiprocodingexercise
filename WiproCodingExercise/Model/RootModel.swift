//
//  RootModel.swift
//  WiproCodingExercise
//
//  Created by Harsh Singh on 19/04/20.
//  Copyright © 2020 Harsh. All rights reserved.
//

import Foundation

// struct for data model member

struct CityData : Codable {
    let title:String?
    var rows:[Item]
}

struct Item : Codable {
    let title : String?
    let description : String?
    let imageHref : String?
}
