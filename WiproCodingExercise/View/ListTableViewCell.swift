//
//  ListTableViewCell.swift
//  WiproCodingExercise
//
//  Created by Harsh Singh on 19/04/20.
//  Copyright © 2020 Harsh. All rights reserved.
//

import UIKit

class ListTableViewCell: UITableViewCell {

    let imgUser = UIImageView()
    let lblTitle = UILabel()
    let lblDescription = UILabel()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setupAllSubViews()
    }

    // MARK: - Setup custom cell all subviews
    func setupAllSubViews() -> Void{
        let marginGuide = contentView.layoutMarginsGuide

        contentView.addSubview(imgUser)
        imgUser.contentMode = .scaleAspectFit
        imgUser.translatesAutoresizingMaskIntoConstraints = false
        imgUser.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        imgUser.topAnchor.constraint(equalTo: marginGuide.topAnchor).isActive = true
        imgUser.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        imgUser.heightAnchor.constraint(greaterThanOrEqualToConstant:250).isActive = true

        // configure lblTitleLabel
        contentView.addSubview(lblTitle)
        lblTitle.translatesAutoresizingMaskIntoConstraints = false
        lblTitle.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        lblTitle.topAnchor.constraint(equalTo: imgUser.bottomAnchor).isActive = true
        lblTitle.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        lblTitle.font = UIFont(name: "Helvetica", size: 20)
        lblTitle.numberOfLines = 0

        // configure lblTitlelblDescriptionription
        contentView.addSubview(lblDescription)
        lblDescription.translatesAutoresizingMaskIntoConstraints = false
        lblDescription.leadingAnchor.constraint(equalTo: marginGuide.leadingAnchor).isActive = true
        lblDescription.bottomAnchor.constraint(equalTo: marginGuide.bottomAnchor).isActive = true
        lblDescription.trailingAnchor.constraint(equalTo: marginGuide.trailingAnchor).isActive = true
        lblDescription.topAnchor.constraint(equalTo: lblTitle.bottomAnchor).isActive = true
        lblDescription.numberOfLines = 0
        lblDescription.font = UIFont(name: "Helvetica", size: 16)
        lblDescription.textColor = .gray
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
