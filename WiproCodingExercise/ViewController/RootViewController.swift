//
//  RootViewController.swift
//  WiproCodingExercise
//
//  Created by Harsh Singh on 15/04/20.
//  Copyright © 2020 Harsh. All rights reserved.
//

import UIKit

class RootViewController: UIViewController {

    // MARK :- properties
    private var isReachableConnection: Bool = false
    var rootViewModel:RootViewModel?
    private var refreshControl = UIRefreshControl()
    let network: NetworkManager = NetworkManager.sharedInstance
    private var messageLabel: UILabel = UILabel()

    let indicator:UIActivityIndicatorView = {
        let indicator = UIActivityIndicatorView(frame: CGRect(x:0, y:0, width:60, height:60))
        if #available(iOS 13.0, *) {
            indicator.style = .large
        } else {
            indicator.style = .gray
        }
        return indicator
    }()

    let listTableView:UITableView = {
        let listTableView = UITableView(frame: .zero, style: .plain)
        listTableView.tableFooterView = UIView(frame: .zero)
        return listTableView
    }()

    init(rootViewModel: RootViewModel) {
        super.init(nibName: nil, bundle: nil)
        self.rootViewModel = rootViewModel
        self.rootViewModel?.delegate = self
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        self.checkInternetConnection()

        self.setupTableViewLayout()

        // Load content
        self.rootViewModel?.getData()
    }

    @objc func refresh(sender:AnyObject) {
        if self.isReachableConnection {
            self.showMainPage()
        } else {
            self.showOfflinePage()
        }
    }

    func setupTableViewLayout(){
        self.indicator.startAnimating()
        
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(RootViewController.refresh(sender:)), for: .valueChanged)
        listTableView.frame = view.bounds
        
        listTableView.delegate = self
        listTableView.dataSource = self
        listTableView.register(ListTableViewCell.self, forCellReuseIdentifier: TableViewKeysConstant.cellReuseIdentifier)
        listTableView.allowsSelection = false
        listTableView.estimatedRowHeight = CGFloat(TableViewKeysConstant.cellEstimatedHeight)
        listTableView.rowHeight = UITableView.automaticDimension
        
        self.view.addSubview(listTableView)
        self.listTableView.addSubview(self.refreshControl)
        self.setLayoutConstraints()
        
        messageLabel.textAlignment = .center
        messageLabel.text = DefaultValueConstant.tableviewEmptyMessage
        messageLabel.numberOfLines = 0
        messageLabel.textColor = .lightGray
        messageLabel.font = UIFont.systemFont(ofSize: 22)
        messageLabel.isHidden = true
        self.view.addSubview(messageLabel)
        
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        messageLabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        messageLabel.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        messageLabel.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        messageLabel.centerXAnchor.constraint(equalTo: messageLabel.superview!.centerXAnchor).isActive = true
        messageLabel.centerYAnchor.constraint(equalTo: messageLabel.superview!.centerYAnchor).isActive = true
        
        indicator.center = self.listTableView.center
        self.view.addSubview(indicator)
    }

    func setLayoutConstraints() {
        listTableView.translatesAutoresizingMaskIntoConstraints = false

        listTableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        listTableView.leftAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor).isActive = true
        listTableView.rightAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor).isActive = true
        listTableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: - extension for TableView

extension RootViewController:UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rootViewModel?.items.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TableViewKeysConstant.cellReuseIdentifier) as? ListTableViewCell
        let item = rootViewModel?.items[indexPath.row]
        cell?.lblTitle.text = item?.title
        cell?.lblDescription.text = item?.description
        cell?.imgUser.image = UIImage(named: TableViewKeysConstant.cellDefaultImage)
        if let imagePath = item?.imageHref {
            rootViewModel?.obtainImageWithPath(imagePath: imagePath) { image in
                if let imageReceived = image, let updateCell = tableView.cellForRow(at: indexPath) as? ListTableViewCell {
                    updateCell.imgUser.image = imageReceived
                }
            }
        }
        return cell!
    }
}

// MARK: - extension for root view model delegate

extension RootViewController: RootViewModelDelegate {
    
    func dataUpdated() {
        DispatchQueue.main.async { [weak self] in
            self?.listTableView.reloadData()
            self?.navigationItem.title = self?.rootViewModel?.title
            self?.indicator.stopAnimating()
            self?.indicator.hidesWhenStopped = true
            self?.refreshControl.endRefreshing()
        }
    }
}

// MARK: - extension for Network checks

extension RootViewController {
    
    // MARK:- check internet connection method
    
    func checkInternetConnection() -> Void {
        NetworkManager.isUnreachable { _ in
            self.showOfflinePage()
        }
        network.reachability.whenUnreachable = { reachability in
            self.showOfflinePage()
        }
        network.reachability.whenReachable = { reachability in
            self.showMainPage()
        }
    }
    
    // MARK: -  Reachability methods
    
    private func showOfflinePage() -> Void {
        self.showDisplayErrorMssage()
        DispatchQueue.main.async {  [weak self] in
            self?.rootViewModel?.items.removeAll()
            self?.rootViewModel?.title.removeAll()
            self?.refreshControl.endRefreshing()
            self?.navigationItem.title = self?.rootViewModel?.title
            guard let table = self?.listTableView else { return }
            table.reloadData()
            self?.messageLabel.isHidden = false
        }
        self.isReachableConnection = false
    }
    
    private func showMainPage() -> Void {
        
        DispatchQueue.main.async { [weak self] in
            self?.rootViewModel?.getData()
            self?.listTableView.reloadData()
        }
        self.messageLabel.isHidden = true
        self.isReachableConnection = true
    }
    
    private func showDisplayErrorMssage () {
        let alertView = UIAlertController(title: ErrorMessageConstant.messageTitle, message: ErrorMessageConstant.messageText, preferredStyle: .alert)
        alertView.addAction(UIAlertAction(title: ErrorMessageConstant.messageActionText, style: .default, handler: nil))
        self.present(alertView, animated: true, completion: nil)
    }
}
