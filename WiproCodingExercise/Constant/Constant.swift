//
//  Constant.swift
//  WiproCodingExercise
//
//  Created by Harsh Singh on 20/04/20.
//  Copyright © 2020 Harsh. All rights reserved.
//

import Foundation

/// Base URL
struct ApiConstant {
    static let baseUrl = "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json"
}

/// Constaint for table view
struct TableViewKeysConstant {
    static let cellReuseIdentifier = "customCell"
    static let cellEstimatedHeight = 44
    static let cellDefaultImage = "placeholder"
}

/// Constaint for API success response 
struct ApiResponseCodeApiConstant {
    static let Success = 200
}

/// Constaint for Error message
struct ErrorMessageConstant {
    static let messageText = "You must connect to Wi-Fi or a Cellular Network to get online Again"
    static let messageTitle = "Message"
    static let messageActionText = "Ok"
}

struct DefaultValueConstant {
    static let text = ""
    static let tableviewEmptyMessage = "Something went wrong with internet. Please check your internet connection and try again"
}
