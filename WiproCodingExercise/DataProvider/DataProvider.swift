//
//  DataProvider.swift
//  WiproCodingExercise
//
//  Created by Harsh Singh on 19/04/20.
//  Copyright © 2020 Harsh. All rights reserved.
//

import Foundation

/// Data Provider class for parsing

final class DataProvider {
    
    let defaultSession = URLSession(configuration: .default)
    var dataTask: URLSessionDataTask?
    static public let shared: DataProvider = DataProvider()
    let jsonUrlString = ApiConstant.baseUrl
    
    func fetchDataForFacts( completionHandler: @escaping(Result<CityData?, Error>) -> Void) {
        
        guard let url = URL(string: jsonUrlString) else { return }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = "GET"
        
        dataTask?.cancel()
        dataTask = defaultSession.dataTask(with: urlRequest.url!) { data, response, error in
            
            defer {
                self.dataTask = nil
            }
            
            if let error = error {
                completionHandler(.failure(error))
                print("[API] Request failed with error: \(error.localizedDescription)")
                return
            }
            
            guard let data = data, let response = response as? HTTPURLResponse else {
                print("[API] equest returned an invalid response")
                return
            }
            
            guard response.statusCode == ApiResponseCodeApiConstant.Success else {
                print("[API] Request returned an unsupported status code: \(response.statusCode)")
                return
            }
            
            let responseStrInISOLatin = String(data: data, encoding: String.Encoding.isoLatin1)
            guard let modifiedDataInUTF8Format = responseStrInISOLatin?.data(using: String.Encoding.utf8) else {
                print("could not convert data to UTF-8 format")
                return
            }
                    
            do {
                let model = try JSONDecoder().decode(CityData.self, from: modifiedDataInUTF8Format)
                completionHandler(.success(model))
            } catch  let error {
                completionHandler(.failure(error))
                print("[API] Decoding failed with error: \(error)")
            }
        }
        dataTask?.resume()
    }
    
    func obtainImageDataWithPath(imagePath: String, completionHandler: @escaping (Data?) -> Void) {
        let url: URL! = URL(string: imagePath)
        let task = defaultSession.dataTask(with: url) { data, response, error in
            guard let data = data, error == nil else { return }
            completionHandler(data)
        }
        task.resume()
    }
}
