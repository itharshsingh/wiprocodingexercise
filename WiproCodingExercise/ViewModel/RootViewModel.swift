//
//  RootViewModel.swift
//  WiproCodingExercise
//
//  Created by Harsh Singh on 19/04/20.
//  Copyright © 2020 Harsh. All rights reserved.
//

import Foundation
import UIKit

/// Protocal for update data to root view controller
protocol RootViewModelDelegate: class {
    func dataUpdated()
}

final class RootViewModel {
    public weak var delegate: RootViewModelDelegate?

    var title:String = ""
    var items: [Item] = [] {
        didSet {
            delegate?.dataUpdated()
        }
    }
    var cache:NSCache<NSString, UIImage> = NSCache()

    func getData() {
        DataProvider.shared.fetchDataForFacts { (result) in
            switch result {
            case .success(let data):
                guard let dataReceived = data, let title = data?.title else {
                    return
                }
                self.title = title
                self.items = dataReceived.rows
            case .failure(let error):
                print("failed to fetch cources:", error )
            }
        }
    }

    func obtainImageWithPath(imagePath: String, completionHandler: @escaping (UIImage?) -> Void) {
        if let image = self.cache.object(forKey: imagePath as NSString) {
            DispatchQueue.main.async {
                completionHandler(image)
            }
        } else {
            DataProvider.shared.obtainImageDataWithPath(imagePath: imagePath, completionHandler: {(data) in
                guard let imageData = data else {
                    return
                }
                if let image = UIImage(data: imageData) {
                    self.cache.setObject(image, forKey: imagePath as NSString)
                    DispatchQueue.main.async {
                        completionHandler(image)
                    }
                }
            })
        }
    }
}
