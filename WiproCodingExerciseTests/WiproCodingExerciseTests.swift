//
//  WiproCodingExerciseTests.swift
//  WiproCodingExerciseTests
//
//  Created by Harsh Singh on 15/04/20.
//  Copyright © 2020 Harsh. All rights reserved.
//

import XCTest
@testable import WiproCodingExercise

class WiproCodingExerciseTests: XCTestCase {
    
    let viewModel = RootViewModel()
    var tableView: UITableView!
    var dataSource: UITableViewDataSource!
    var rootViewModel: RootViewModel!
    let controller = RootViewController(rootViewModel: RootViewModel())
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        controller.loadViewIfNeeded()
        controller.setupTableViewLayout()
        tableView = controller.listTableView
        
        guard let ds = tableView.dataSource else {
            return XCTFail("Controller's table view should have a data source")
        }
        dataSource = ds
        
        for number in 0..<14 {
            controller.rootViewModel?.title.append("canada: \(number)")
        }
        
    }
    
    func testTableViewHasCells() {
        let cell = tableView.dequeueReusableCell(withIdentifier: "customCell") as! ListTableViewCell
        XCTAssertNotNil(cell,
                        "TableView should be able to dequeue cell with identifier: 'Cell'")
    }
    
    func testCellLabelHasTitle() {
        let customCell = tableView.dequeueReusableCell(withIdentifier: "customCell") as! ListTableViewCell
        XCTAssertEqual(customCell.lblTitle.text, nil,
                       "No description found for title")
    }
    
    func testNavigationHasTitle() {
        XCTAssertEqual(controller.title, nil,
                       "No description found for title")
    }
    
    func testForPlaceholderImage() {
        let image = UIImage(named: "placeholder.png")
        XCTAssertNotNil(image)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        tableView = nil
        dataSource = nil
        rootViewModel = nil
        controller.rootViewModel = nil
        super.tearDown()
    }
}
