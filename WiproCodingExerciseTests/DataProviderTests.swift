//
//  DataProviderTests.swift
//  WiproCodingExerciseTests
//
//  Created by Harsh Singh on 20/04/20.
//  Copyright © 2020 Harsh. All rights reserved.
//

import XCTest
@testable import WiproCodingExercise

class DataProviderTests: XCTestCase {
    
    let dataProvider = DataProvider()
    
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    
    func testGettingJSON() {
        let expect = expectation(description: "Expecting a JSON data not nil")
        dataProvider.fetchDataForFacts { result in
            switch result {
            case .success(let response):
                XCTAssertTrue((response != nil))
                expect.fulfill()
            default:
                return
            }
        }
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testAsyncCalbackfailure() {
        let expect = expectation(description: "Expecting a JSON data not nil")
        dataProvider.fetchDataForFacts { result in
            switch result {
            case .success(let response):
                XCTAssertFalse((response == nil))
                expect.fulfill()
            default:
                return
            }
        }
        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
    
    func testAsyncCalbackDataResponse() {
        dataProvider.fetchDataForFacts { result in
            switch result {
            case .success(let response):
                XCTAssertEqual(response?.title, "About Canada", "Expected About Canada base")
                if let list = response?.rows {
                    XCTAssertEqual(list.count, 14, "Expected 14 rates")
                }
            default:
                return
            }
        }
    }
    
    func testFetchNoFeedsFailure() {
        dataProvider.fetchDataForFacts { result in
            switch result {
            case .failure:
                XCTAssert(false, "Expected failure when wrong data")
            default:
                return
            }
        }
    }    
}
